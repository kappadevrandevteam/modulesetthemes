{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

<div class="row" >
    <div class="col-md-6">
        <div id="">
            {if $listing.pagination.items_shown_from == 1}
                <div class="block-category card card-block">
                    <h1 class="h1">{$category.name}</h1>
                    <!--<div class="block-category-inner">
                        {if $category.description}
                            <div id="category-description" class="text-muted">{$category.description nofilter}</div>
                        {/if}
                        {if $category.image.large.url}
                            <div class="category-cover">
                                <img src="{$category.image.large.url}" alt="{if !empty($category.image.legend)}{$category.image.legend}{else}{$category.name}{/if}">
                            </div>
                        {/if}
                    </div>-->
                </div>
            {/if}
        </div>
    </div>
    <div class="col-md-6"> 
        <div class="row" >
            <div class="col-md-6">

                <div class="{if !empty($listing.rendered_facets)}col-sm-9 col-xs-8{else}col-sm-12 {/if} col-md-12 products-sort-order dropdown">
                    <button
                        class="btn-unstyle select-title"
                        rel="nofollow"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false">
                        {if isset($listing.sort_selected)}{$listing.sort_selected}{else}{l s='Select' d='Shop.Theme.Actions'}{/if}
                        <i class="material-icons float-xs-right">&#xE5C5;</i>
                    </button>
                    <div class="dropdown-menu">
                        {foreach from=$listing.sort_orders item=sort_order}
                        <a
                            rel="nofollow"
                            href="{$sort_order.url}"
                            class="select-list {['current' => $sort_order.current, 'js-search-link' => true]|classnames}"
                        >
                            {$sort_order.label}
                        </a>
                        {/foreach}
                    </div>
                </div>

            </div>

            <div class="col-md-6">

                 {if isset($subcategories)}
                <!-- Subcategories -->
                <div id="subcategoriesbox">
                    <!--<p class="subcategory-heading">{l s='Subcategories'}</p>-->
                    <button
                        class="btn-unstyle select-title"
                        rel="nofollow"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false">
                        {$category.name}
                        <i class="material-icons float-xs-right">&#xE5C5;</i>
                    </button>
                        <div class="dropdown-menu">
                            {foreach from=$subcategories item=subcategory}
                                <h5><a class="subcategory-name" href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}">{$subcategory.name|truncate:25:'...'|escape:'html':'UTF-8'}</a></h5>
                            
                            {/foreach}
                        </div>
                </div>
                {/if}

            </div>

        </div>
    </div>
</div>